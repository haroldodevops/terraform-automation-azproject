resource "azuredevops_project" "harolgyver" {
  name               = var.project_name
  visibility         = "private"
  version_control    = var.version_control
  work_item_template = var.work_item_template
  description        = "Managed by Terraform"
  # Enable or desiable the DevOps fetures below (enabled / disabled)
  features = {
      "boards"       = "enabled"
      "repositories" = "enabled"
      "pipelines"    = "enabled"
      "testplans"    = "enabled"
      "artifacts"    = "enabled"
  }
}