
# Terraform Automation Project

This is an automation project using Terraform to create projects, repositories, and service connections in Azure DevOps. The service connections include SonarCloud, Docker Registry, and Azure Resource Manager (Azure RM). Additionally, existing members and groups can be added to the new projects.

## Instructions to Create a New Project:

1. **Update the `terraform.tfvars` File:** Edit this file by inserting the name of the new project and repository you wish to create.

    ```hcl
    project_name    = "YourNewProject"
    repository_name = "YourNewRepository"
    ```

2. **Modify Group Members (Optional):** If you need to change the group members, make the changes in the section of the `az devops security group membership add` command. Update the `--member-id` parameter with the name of the new group or member you desire.

    ```bash
    az devops security group membership add --group-id=$(ContributorsDescriptor) --member-id "YourNewGroupOrMember" --output table
    ```


---

# Projeto de Automação com Terraform

Este é um projeto de automação utilizando Terraform para criar projetos, repositórios e conexões de serviço no Azure DevOps. As conexões de serviço incluem SonarCloud, Docker Registry e Azure Resource Manager (Azure RM). Além disso, membros e grupos existentes podem ser adicionados aos novos projetos.

## Instruções para Criar um Novo Projeto:

1. **Atualize o Arquivo `terraform.tfvars`:** Edite este arquivo inserindo o nome do novo projeto e repositório que deseja criar.

    ```hcl
    project_name    = "SeuNovoProjeto"
    repository_name = "SeuNovoRepositorio"
    ```

2. **Modifique os Membros do Grupo (Opcional):** Caso seja necessário alterar os membros do grupo, faça as alterações na seção do comando `az devops security group membership add`. Atualize o parâmetro `--member-id` com o nome do novo grupo ou membro desejado.

    ```bash
    az devops security group membership add --group-id=$(ContributorsDescriptor) --member-id "SeuNovoGrupoOuMembro" --output table
    ```