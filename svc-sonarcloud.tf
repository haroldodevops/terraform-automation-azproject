data "azurerm_key_vault_secret" "sonarcloud_token" {
  name         = "SonarCloudToken"
  key_vault_id = "/subscriptions/SuaSubscriptionID/resourceGroups/SuoResourceGroup/providers/Microsoft.KeyVault/vaults/SeuKeyVault"
}


resource "azuredevops_serviceendpoint_sonarcloud" "sonarcloud" {
  project_id            = azuredevops_project.harolgyver.id
  service_endpoint_name = "SonarCloud"
  token                 = data.azurerm_key_vault_secret.sonarcloud_token.value
  description           = "Managed by Terraform"
}