# Data block to get secrets from Key Vault
data "azurerm_key_vault_secret" "docker_registry_credentials" {
  name         = "ArtifactRegistryToken"
  key_vault_id = "/subscriptions/SuaSubscriptionID/resourceGroups/SuoResourceGroup/providers/Microsoft.KeyVault/vaults/SeuKeyVault"
}

# docker  registry service connection
resource "azuredevops_serviceendpoint_dockerregistry" "artifact-registry" {
  project_id            = azuredevops_project.harolgyver.id
  service_endpoint_name = "harolgyver-registry"
  docker_registry       = "https://docker.io"
  description           = "Managed by Terraform"
  docker_username       = "_json_key"
  docker_password       = data.azurerm_key_vault_secret.docker_registry_credentials.value
  registry_type         = "Others"
}