resource "azuredevops_git_repository" "harolgyver" {
  project_id         = azuredevops_project.harolgyver.id
  name               = var.repository_name
  initialization {
    init_type = "Clean"
  }
}
