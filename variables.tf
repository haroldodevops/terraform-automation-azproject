# vriaveis da crição do projeto 
variable "project_name" {
  default = ""
}
variable "version_control" {
  default = "Git"
}

variable "work_item_template" {
  default = "Agile"
}

# variavel Nome do repositorio git 
variable "repository_name" {
  default = ""
}