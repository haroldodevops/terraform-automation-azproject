# Data block to get secrets from Key Vault
data "azurerm_key_vault_secret" "azurerm_spn_key" {
  name         = "azure-devops-credentials"
  key_vault_id = "/subscriptions/SuaSubscriptionID/resourceGroups/SuoResourceGroup/providers/Microsoft.KeyVault/vaults/SeuKeyVault"
}
# Azure service connection
resource "azuredevops_serviceendpoint_azurerm" "azurerm" {
  project_id                = azuredevops_project.harolgyver.id
  service_endpoint_name     = "Azure-Connection"
  description               = "Managed by Terraform"
  credentials {
    serviceprincipalid      = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
    serviceprincipalkey     = data.azurerm_key_vault_secret.azurerm_spn_key.value
  }
  azurerm_spn_tenantid      = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
  azurerm_subscription_id   = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
  azurerm_subscription_name = "pay-as-you-go"
}