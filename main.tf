terraform {
  required_providers {
    google = {
      version = "~> 5.5.0"
    }
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 3.78.0"
    }
    azuredevops = {
      source  = "microsoft/azuredevops"
      version = "~> 0.10.0"
    }
  }
}

provider "google" {
  project     = "gcp-harolgyver"
  region      = "us-central1"
  zone        = "us-central1-a"
}

provider "azurerm" {
  features {}
}

provider "azuredevops" {
  org_service_url = "https://dev.azure.com/harolgyver"
}